#ifndef CELL_H
#define CELL_H
#include <QAbstractListModel>
#include <QList>
#include <QObject>
#include <QPoint>

class Cell : public QObject {
    Q_OBJECT

public:
    Q_DISABLE_COPY(Cell)
    Cell(QObject* const flyModeling,
         QPoint position,
         int cellSize);
    ~Cell() = default;
    bool add(QObject* const fly);
    bool move(QObject* const fly, Cell* const toCell);
    bool remove(QObject* const fly);

    //interface QML
    Q_PROPERTY(QPoint position READ position)
    Q_PROPERTY(bool full READ isFull NOTIFY fullChanged)
    Q_PROPERTY(QList<QObject*> model READ model NOTIFY modelChanged)

    QPoint position() const;
    bool isFull() const;
    QList<QObject*> model() const;
signals:
    void fullChanged(bool full);
    void modelChanged(QList<QObject*>);

private:
    QList<QObject*> m_model{};
    QPoint m_position{};
    int m_cellSize{};
};

#endif // CELL_H
