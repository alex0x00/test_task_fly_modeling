#include "flymodel.h"

FlyModel::FlyModel(FlyModeling* const parent)
    : QAbstractListModel(parent)
{
    Q_ASSERT(parent != nullptr);
    auto M = parent->fieldSize();
    auto N = parent->cellSize();
    m_fild = std::vector<Cell*>(M * M);

    for (size_t k{}; k < M * M; ++k) {
        m_fild[k] = new Cell(this, QPoint(k / M, k % M), N);
    }
}

bool FlyModel::addFly(QPoint position)
{
    auto cell_on_pos{ cell(position) };
    if (cell_on_pos) {
        if (!cell_on_pos->isFull()) {
            auto fly{ makeFly(position) };
            return cell_on_pos->add(fly);
        }
    }
    return false;
}

Fly* FlyModel::makeFly(QPoint position)
{
    double r = double(qrand()) / RAND_MAX;
    auto modeling{ flyModeling() };
    int T = std::ceil(r * (modeling->maxT() - modeling->minT())
        + modeling->minT());
    auto fly = new Fly(
        position,
        T,
        modeling->timeStep(),
        modeling->fieldSize() * T,
        modeling->isActive());
    connect(modeling, &FlyModeling::pauseChanged, fly, &Fly::pause);
    connect(fly, &Fly::move, this, &FlyModel::moveFly);
    m_statistics.append(fly);
    return fly;
}

Cell* FlyModel::cell(QPoint pos)
{
    Cell* ret{};
    auto M = flyModeling()->fieldSize();
    int x{ pos.rx() }, y{ pos.ry() };
    if (x >= 0
        && y >= 0
        && x < M
        && y < M) {
        ret = m_fild[x * M + y];
    }
    return ret;
}

QList<QObject*> FlyModel::statistics()
{
    return m_statistics;
}

void FlyModel::moveFly(Fly* const fly, QPoint position)
{
    if (fly) {
        auto from_cell{ cell(fly->position()) };
        auto to_cell{ cell(position) };
        if (from_cell && to_cell) {
            if (from_cell->move(fly, to_cell)) {
                fly->moved(position);
            }
        }
    }
}

FlyModeling* FlyModel::flyModeling() const
{
    return static_cast<FlyModeling*>(parent());
}

int FlyModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return m_fild.size();
}

QVariant FlyModel::data(const QModelIndex& index, int role) const
{
    Q_UNUSED(role)
    auto M = flyModeling()->fieldSize();
    if (index.row() < 0 || index.row() >= M * M)
        return QVariant();

    Cell* it = m_fild[index.row()];
    switch (role) {
    case cellRole:
        return QVariant::fromValue(it);
    case positionRole:
        return it->position();
    case fullRole:
        return it->isFull();
    }
    return QVariant();
}

QHash<int, QByteArray> FlyModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[cellRole] = "cell";
    roles[positionRole] = "position";
    roles[fullRole] = "full";
    return roles;
}
