#ifndef FLY_H
#define FLY_H
#include <QObject>
#include <QPoint>
#include <QTimer>

class Fly : public QObject {
    Q_OBJECT
public:
    Q_DISABLE_COPY(Fly)

    Fly(QPoint position,
        int intellect,
        int time_step,
        int age,
        bool is_active);
    ~Fly() = default;
    void stroke();
    void moved(QPoint new_position); //смена позиции

    //interface QML
    Q_PROPERTY(int intellect READ intellect NOTIFY intellectChanged)
    Q_PROPERTY(float speed READ speed NOTIFY speedChanged)
    Q_PROPERTY(int mileage READ mileage NOTIFY mileageChanged)
    Q_PROPERTY(int age READ age WRITE setAge NOTIFY ageChanged)
    Q_PROPERTY(bool life READ isLife NOTIFY lifeChanged)
    Q_PROPERTY(QPoint position READ position)
    int intellect() const;
    float speed() const;
    int mileage() const;
    int age() const;
    bool isLife() const;
    QPoint position() const;

public slots:
    void setAge(int age);
    void pause();

signals:
    void intellectChanged(int intellect);
    void speedChanged(float speed);
    void mileageChanged(int mileage);
    void ageChanged(int age);
    void lifeChanged(bool life);
    void move(Fly* fly, QPoint new_position); //сигнал на начало перемещения

private:
    int m_mileage{};
    int m_age{ 1 };
    int m_dead_time{};
    int m_intellect{};
    QTimer m_timer;
    QPoint m_position;
};

#endif // FLY_H
