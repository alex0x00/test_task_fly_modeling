#include "fly.h"
#include <QtCore>
#include <array>

Fly::Fly(QPoint position, int intellect, int time_step, int age, bool is_active)
    : m_dead_time{ age }
    , m_intellect{ intellect }
    , m_position{ position }
{
    m_timer.setInterval(intellect * time_step);
    connect(&m_timer, &QTimer::timeout, this, &Fly::stroke);
    if (is_active) {
        m_timer.start();
    }
}

void Fly::stroke()
{
    static std::array<const QPoint, 8> direction {
        {
            { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 },
            { 1, 1 }, { 1, -1 }, { -1, -1 }, { -1, 1 }
        }
    };

    if (isLife()) {
        auto new_position{ direction[qrand() % direction.size()] };
        emit move(this, m_position + new_position);
        setAge(m_age + 1);
    }
}

void Fly::pause()
{
    if (m_timer.isActive()) {
        m_timer.stop();
    } else {
        m_timer.start();
    }
}

void Fly::moved(QPoint new_position)
{
    m_mileage += (new_position - m_position).manhattanLength();
    m_position = new_position;
    emit mileageChanged(m_mileage);
}

void Fly::setAge(int age)
{
    if (m_age == age)
        return;
    m_age = age;
    emit speedChanged(speed());
    if (!isLife()) {
        m_timer.stop();
        emit lifeChanged(false);
    }
    emit ageChanged(age);
}

float Fly::speed() const
{
    return float(m_mileage) / m_age;
}

int Fly::mileage() const
{
    return m_mileage;
}

int Fly::age() const
{
    return m_age;
}

bool Fly::isLife() const
{
    return m_age <= m_dead_time;
}

QPoint Fly::position() const
{
    return m_position;
}

int Fly::intellect() const
{
    return m_intellect;
}
