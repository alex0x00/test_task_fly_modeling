#ifndef FLYMODEL_H
#define FLYMODEL_H
#include "cell.h"
#include "fly.h"
#include "flymodeling.h"
#include <QAbstractListModel>
#include <QObject>

class FlyModeling;

class FlyModel : public QAbstractListModel {
    Q_OBJECT
public:
    Q_DISABLE_COPY(FlyModel)
    enum CellRoles {
        cellRole = Qt::UserRole + 1,
        positionRole,
        fullRole
    };

    explicit FlyModel(FlyModeling* const parent = nullptr);
    ~FlyModel() = default;

    Fly* makeFly(QPoint position);
    Cell* cell(QPoint position);
    FlyModeling* flyModeling() const;
    QList<QObject*> statistics();

    //interface QML
    Q_INVOKABLE bool addFly(QPoint position);

    int rowCount(const QModelIndex& parent) const;
    QVariant data(const QModelIndex& index, int role) const;
    QHash<int, QByteArray> roleNames() const;
public slots:
    void moveFly(Fly* const fly, QPoint position);

private:
    std::vector<Cell*> m_fild;
    QList<QObject*> m_statistics;
};
#endif // FLYMODEL_H
