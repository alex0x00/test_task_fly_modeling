#include "cell.h"
#include <algorithm>

Cell::Cell(QObject* const flyModeling, QPoint position, int cellSize)
    : QObject{ flyModeling }
    , m_position{ position }
    , m_cellSize{ cellSize }
{
}

bool Cell::add(QObject* const fly)
{
    if (!isFull() && fly) {
        fly->setParent(this);
        m_model.push_back(fly);
        emit modelChanged(model());
        if (isFull()) {
            emit fullChanged(true);
        }
        return true;
    }
    return false;
}

bool Cell::move(QObject* const fly, Cell* const toCell)
{
    if (toCell) {
        if (this == toCell) {
            return true;
        }
        if (toCell->add(fly)) {
            return remove(fly);
        }
    }
    return false;
}

bool Cell::remove(QObject* const fly)
{
    auto it{std::remove(m_model.begin(), m_model.end(), fly)};
    bool is_remove{it != m_model.end()};
    m_model.erase(it, m_model.end());
    if (is_remove) {
        emit modelChanged(model());
        if (!isFull()) {
            emit fullChanged(false);
        }
        return true;
    }
    Q_ASSERT_X(false, "remove", "no removed item");
    return false;
}

QPoint Cell::position() const
{
    return m_position;
}

bool Cell::isFull() const
{
    return m_model.size() == m_cellSize;
}

QList<QObject*> Cell::model() const
{
    return m_model;
}
