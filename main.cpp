#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include "fly.h"
#include "cell.h"
#include "flymodel.h"
#include "flymodeling.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    qmlRegisterUncreatableType<Fly>("ru.yandex.kuznecov0x00", 1, 0, "Fly",
                                    "create component Fly native only");
    qmlRegisterUncreatableType<Cell>("ru.yandex.kuznecov0x00", 1, 0, "Cell",
                                     "create component Cell native only");
    qmlRegisterUncreatableType<FlyModel>("ru.yandex.kuznecov0x00", 1, 0, "FlyModel",
                                         "create component FlyModel native only");
    qmlRegisterType<FlyModeling>("ru.yandex.kuznecov0x00", 1, 0, "FlyModeling");
    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
