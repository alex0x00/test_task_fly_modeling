import QtQuick 2.0

Flickable{
    id: flickable
    contentWidth: grid.width
    contentHeight: grid.height
    property var model
    property alias contentScale: grid.scale
    property int fieldSize
    Image {
        id: img
        source: "oranges.png"
        width: grid.width
        height: grid.height
        Grid{
            id: grid;
            rows: fieldSize
            columns: fieldSize
            spacing: 4
            Repeater{
                model: flickable.model
                delegate: CellDelegate{
                    id: cellDelegate
                    cell: model.cell
                }
            }
        }
    }
}

