import QtQuick 2.0

Item {
    id:item
    height: size * length
    width: size * length
    property int length: 30
    property int size: Math.ceil(Math.sqrt(flyModeling.cellSize));
    property var cell

    Rectangle{
        id:rect
        anchors.fill: parent
        border.width: 2
        radius: 4
        height: size * length
        width: size * length
        opacity: mouseArea.containsMouse ? 1.0: 0.3
        color: "transparent"
    }

    state: cell.full ? "Full": "NotFull";

    MouseArea{
        id:mouseArea
        anchors.fill: parent;
        hoverEnabled: true;
        onClicked: {
            fieldView.model.addFly(cell.position);
        }
    }

    Flow {
        id: grid
        anchors.fill: parent
        Repeater{
            model: cell.model;
            Image{
                source: "fly.png"
                height: item.length
                width: item.length
                scale: modelData.life ? 1.0: 0.7
            }
        }
    }

    states: [
        State {
            name: "NotFull"
            PropertyChanges {
                target: rect
                border.color: "red"
            }
        },
        State {
            name: "Full"
            PropertyChanges {
                target: rect
                border.color: "gray"
            }
        }
    ]
}
