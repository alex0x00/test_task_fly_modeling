#ifndef FLYMODELING_H
#define FLYMODELING_H
#include <QObject>
#include <QPoint>

class FlyModel;

class FlyModeling : public QObject {
    Q_OBJECT
public:
    Q_DISABLE_COPY(FlyModeling)
    explicit FlyModeling(QObject* parent = nullptr);
    ~FlyModeling() = default;

    //interface QML
    Q_INVOKABLE bool create(int fieldSize,
                            int cellSize,
                            int timeStep,
                            int minT,
                            int maxT);
    Q_INVOKABLE bool pause();
    Q_INVOKABLE void newStatistic();
    Q_PROPERTY(QList<QObject*> statistics READ statistics NOTIFY statisticsChanged)
    Q_PROPERTY(FlyModel* model READ model NOTIFY modelChanged)
    Q_PROPERTY(int fieldSize READ fieldSize NOTIFY fieldSizeChanged)
    Q_PROPERTY(int cellSize READ cellSize NOTIFY cellSizeChanged)
    Q_PROPERTY(int timeStep READ timeStep)
    Q_PROPERTY(int minT READ minT)
    Q_PROPERTY(int maxT READ maxT)
    Q_PROPERTY(bool active READ isActive NOTIFY activeChanged)

    FlyModel* model() const;
    int fieldSize() const;
    int cellSize() const;
    int timeStep() const;
    int minT() const;
    int maxT() const;
    bool isActive() const;
    QList<QObject*> statistics() const;
public slots:
    void setFieldSize(int fieldSize);

signals:
    void statisticsChanged(QList<QObject*> statistics);
    void modelChanged(FlyModel*);
    void fieldSizeChanged(int);
    void cellSizeChanged(int);
    void pauseChanged();
    void activeChanged(bool active);

private:
    FlyModel* m_flyModel{};
    int m_fieldSize{ 1 };
    int m_cellSize{ 1 };
    int m_timeStep{ 1000 };
    int m_minT{ 1 };
    int m_maxT{ 2 };
    bool m_active{ false };
};

#endif // FLYMODELING_H
