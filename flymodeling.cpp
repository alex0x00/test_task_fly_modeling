#include "flymodeling.h"
#include "flymodel.h"
#include <cstdlib>
#include <QDateTime>

FlyModeling::FlyModeling(QObject *parent)
    :QObject(parent)
{
    qsrand(QDateTime::currentMSecsSinceEpoch());
}

bool FlyModeling::create(
        int fieldSize,
        int cellSize,
        int timeStep,
        int minT,
        int maxT)
{
    if (!m_flyModel
            && fieldSize > 0
            && cellSize > 0
            && timeStep > 0
            && minT > 0
            && maxT > minT)
    {
        m_timeStep = timeStep;
        m_minT = minT;
        m_maxT = maxT;
        m_cellSize = cellSize;
        setFieldSize(fieldSize);
        m_flyModel = new FlyModel(this);
        emit cellSizeChanged(cellSize);
        emit modelChanged(m_flyModel);
        return true;
    }
    return false;
}

void FlyModeling::newStatistic(){
    emit statisticsChanged(statistics());
}

QList<QObject *> FlyModeling::statistics() const
{
    if (m_flyModel){
        return m_flyModel->statistics();
    }
    return {};
}

int FlyModeling::fieldSize() const
{
    return m_fieldSize;
}

int FlyModeling::cellSize() const
{
    return m_cellSize;
}

int FlyModeling::timeStep() const
{
    return m_timeStep;
}

int FlyModeling::minT() const
{
    return m_minT;
}

int FlyModeling::maxT() const
{
    return m_maxT;
}

bool FlyModeling::isActive() const
{
    return m_active;
}

FlyModel *FlyModeling::model() const
{
    return m_flyModel;
}

bool FlyModeling::pause(){
    m_active ^= true;
    emit pauseChanged();
    emit activeChanged(m_active);
    return m_active;
}

void FlyModeling::setFieldSize(int fieldSize)
{
    if (m_fieldSize == fieldSize)
        return;
    m_fieldSize = fieldSize;
    emit fieldSizeChanged(fieldSize);
}
