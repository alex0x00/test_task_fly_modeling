import QtQuick 2.4
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2

Dialog {
    id: dialog
    title: qsTr("Настройка модели")
    standardButtons: Dialog.Ok | Dialog.Cancel
    property alias fieldSize: textField1.text
    property alias cellSize: textField2.text
    property alias timeStep: textField3.text
    property int minT: rangeSlider.first.value
    property int maxT: rangeSlider.second.value

    IntValidator{
        id: validatorSize
        bottom: 1
    }
    GridLayout {
        id: grid
        anchors.centerIn: parent
        columns: 2
        Label {
            text: qsTr("Размер поля M:")
            Layout.alignment: Qt.AlignVCenter |
                              Qt.AlignVCenter
        }
        TextField {
            id:textField1
            text: "1"
            validator: validatorSize
        }
        Label {
            text: qsTr("Вместимость ячейки N:")
            Layout.alignment: Qt.AlignVCenter |
                              Qt.AlignVCenter
        }
        TextField {
            id:textField2
            text: "1"
            validator: validatorSize
        }
        Label {
            text: qsTr("Шаг моделирования (мсек):")
            Layout.alignment: Qt.AlignVCenter |
                              Qt.AlignVCenter
        }
        TextField {
            id:textField3
            text: "1000"
            validator: IntValidator{ bottom: 100}
        }

        Label {
            text: qsTr("Диапозон интеллекта T:")
            Layout.alignment: Qt.AlignVCenter |
                              Qt.AlignVCenter
        }

        RangeSlider {
            id: rangeSlider
            from: 1
            to: 100
            stepSize: 1
            first.value: from
            second.value: to
        }
    }
}
