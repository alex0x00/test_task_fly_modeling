import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

Drawer{
    property alias model: listView.model
    ListView{
        id: listView
        anchors.fill: parent;
        delegate: Rectangle{
            width: parent.width
            height: column.height
            border{
                width: 1
                color: "gray"
            }
            ColumnLayout {
                id:column
                Label{
                    text: qsTr("Интеллект: %1").arg(modelData.intellect)
                }
                Label{
                    text: qsTr("Скорость: %1").arg(modelData.speed)
                }
                Label{
                    text: qsTr("Путь: %1").arg(modelData.mileage)
                }
                Label{
                    text: qsTr("Возраст: %1").arg(modelData.age)
                }
                Label{
                    text: qsTr("Живет: %1").arg(
                              modelData.life ? qsTr("да") :qsTr("нет"));
                }
            }
        }
        ScrollBar.vertical: ScrollBar{ }
    }
}
