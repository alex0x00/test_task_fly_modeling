import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import ru.yandex.kuznecov0x00 1.0 as C

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Fly Modeling")
    property bool createdFlyModeling: false;

    C.FlyModeling{
        id: flyModeling
    }

    header: ToolBar {
        RowLayout {
            anchors.fill: parent
            Button {
                id: buttonNewModel
                text: qsTr("Новая модель")
                enabled: !createdFlyModeling
                onClicked: dialog.open()

            }
            Button {
                id: buttonPause
                text: flyModeling.active? qsTr("Остановить") : qsTr("Запустить")
                enabled: createdFlyModeling
                onClicked: flyModeling.pause()
            }
            Button {
                id: buttonStatistics
                text: qsTr("Статистика")
                enabled: createdFlyModeling
                onClicked: {
                    flyModeling.newStatistic();
                    drawer.open();
                }
            }
            Item{
                Layout.fillWidth: true
            }
        }
    }

    CharacteristicDialog{
        id: dialog
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        onAccepted: {
            createdFlyModeling = flyModeling.create(
                        parseInt(fieldSize),
                        parseInt(cellSize),
                        parseInt(timeStep),
                        Math.round(minT),
                        Math.round(maxT));
        }

        onRejected: dialog.close();
    }

    FieldView{
        id: fieldView
        anchors.fill: parent;
        model: flyModeling.model
        fieldSize: flyModeling.fieldSize
    }
    StatisticPanel{
        id: drawer;
        width: 300
        height: parent.height
        model:flyModeling.statistics
    }

}
